console.log("Aquí funcionando con Nodemon")

var movimientosJSON = require('./movimientosv2.json')
var usuariosJSON = require('./usuarios.json')
var express = require('express')
var bodyparser = require('body-parser')
var jsonQuery = require('json-query')
var requestJson = require('request-json')
//var archivoPDF = require('./Actividades_Horarios_CS6_blue.pdf')
var app = express()


app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next()
})

app.use(bodyparser.json())

app.get('/', function(req, res) {
  res.send('Hola API')
})

app.get('/v1/movimientos', function(req, res) {
  res.sendfile('movimientosv1.json')
})

app.get('/v4/archivoPDF', function(req, res) {
  res.sendfile('Actividades_Horarios_CS6_blue.pdf')
})


app.get('/v2/movimientos', function(req, res) {
  res.send(movimientosJSON)
})

app.get('/v2/movimientos/:id', function(req, res) {
  //console.log(req)
  console.log(req.params.id)
  //console.log(movimientosJSON)
  //res.send("Hemos recibido su petición de consulta del movimiento #" + req.params.id)
  res.send(movimientosJSON[req.params.id-1])
})

app.get('/v2/movimientosq', function(req, res) {
  console.log(req.query)
  res.send("recibido")
})

app.get('/v2/movimientosp/:id/:nombre', function(req, res) {
  console.log(req.params)
  res.send("recibido")
})

app.post('/v2/movimientos', function(req, res) {
  //console.log(req)
  //console.log(req.headers['authorization'])
  //if (req.headers['authorization']!=undefined)
  //{
  var nuevo = req.body
  nuevo.id = movimientosJSON.length + 1
  movimientosJSON.push(nuevo)
  res.send("Movimiento dado de alta")
  //}
  //else {
  //  res.send("No está autorizado")
  //}
})

app.put('/v2/movimientos/:id', function(req, res) {
  var cambios = req.body
  var actual = movimientosJSON[req.params.id-1]
  if (cambios.importe != undefined)
  {
    actual.importe = cambios.importe
  }
  if (cambios.ciudad != undefined)
  {
    actual.ciudad = cambios.ciudad
  }
  res.send("Cambios realizados")
})

app.delete('/v2/movimientos/:id', function(req, res) {
  var actual = movimientosJSON[req.params.id-1]
  movimientosJSON.push({
    "id": movimientosJSON.length+1,
    "ciudad": actual.ciudad,
    "importe": actual.importe * (-1),
    "concepto": "Negativo del " + req.params.id
  })
  res.send("Movimiento anulado")
})



app.get('/v2/usuarios/:id', function(req, res) {
  //console.log(req)
  console.log(req.params.id)
  //console.log(movimientosJSON)
  //res.send("Hemos recibido su petición de consulta del movimiento #" + req.params.id)
  res.send(usuariosJSON[req.params.id-1])
})

app.post('/v2/usuarios/login', function(req, res) {
  var nombre = req.headers.usuario
  var password= req.headers.password

    console.log(nombre)
    console.log(password)
  var usr= usuariosJSON.filter(function(usuariosJSON){
  return (usuariosJSON.nombre == nombre && usuariosJSON.password  == password)

  });
  console.log(usr)
  if (usr != ''){
    //sconsole.log(usr[0])
    usr[0].conectado=true
    console.log(usr[0])
      res.send(usr[0])

  }else {
    res.send("usuario "+nombre+ " no encontrado")
  }

})

app.post('/v2/usuarios/logout', function(req, res) {
  var id = req.headers.id

  console.log(id)

  var usr= usuariosJSON.filter(function(usuariosJSON){
  return (usuariosJSON.id == id)
  });
  if(usr[0].conectado==true){
    usr[0].conectado=false
    res.send("usuario "+id+ " desconectado")
  }else {
      res.send("usuario no encontrado")
  }

  console.log(usr)
  if (usr != ''){
    //sconsole.log(usr[0])
    usr[0].conectado=true
    console.log(usr[0])
      res.send(usr[0])

  }else {
    res.send("usuario "+nombre+ " no encontrado")
  }

})
var urlMlabRaiz = "https://api.mlab.com/api/1/databases/techux/collections"
var apiKey = "apiKey=gjeQVZLI7IXAWoT4dGmbEnE8j9lAO90c"
var clienteMlab = requestJson.createClient(urlMlabRaiz + "?" + apiKey)

app.get('/v3', function(req, res) {
  clienteMlab.get('', function(err, resM, body) {
    var coleccionesUsuario = []
    if (!err) {
      for (var i = 0; i < body.length; i++) {
        if (body[i] != "system.indexes") {
          coleccionesUsuario.push({"recurso":body[i], "url":"/v3/" + body[i]})
        }
      }
      res.send(coleccionesUsuario)
    }
    else {
      res.send(err)
    }
  })
})

app.get('/v3/usuarios', function(req, res) {
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    res.send(body)
  })
})

app.get('/v3/movimientos', function(req, res) {
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/movimientos?" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    res.send(body)
  })
})


app.get('/v4/cuentas', function(req, res) {
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    res.send(body)
  })
})

app.get('/v4/cuentas/:id', function(req, res) {
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas")
  clienteMlab.get('?q={"usuario":"' + req.params.id + '"}&f={"movimientos":0,"usuario":0}&' + apiKey,
  function(err, resM, body) {
    res.send(body)
  })
})

app.get('/v4/cuentas/movimientos/:idCuenta', function(req, res) {
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas")
  clienteMlab.get('?q={"idCuenta":' + req.params.idCuenta + '}&' + apiKey,
  function(err, resM, body) {
    res.send(body)
  })
})

app.post('/v4/usuarios', function(req, res) {
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
  clienteMlab.post('', req.body, function(err, resM, body) {
    res.send(body)
  })
})

app.get('/v3/usuarios/:id', function(req, res) {
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
  clienteMlab.get('?q={"idusuario":' + req.params.id + '}&' + apiKey,
  function(err, resM, body) {
    res.send(body)
  })
})


app.get('/v3/movimientos/:id', function(req, res) {
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/movimientos")
  clienteMlab.get('?q={"idcuenta":"' + req.params.id + '"}&' + apiKey,
  function(err, resM, body) {
    res.send(body)
  })
})


app.post('/v3/usuarios', function(req, res) {
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
  clienteMlab.post('', req.body, function(err, resM, body) {
    res.send(body)
  })
})

app.put('/v3/usuarios/:id', function(req, res) {
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
  var cambio = '{"$set":' + JSON.stringify(req.body) + '}'
  clienteMlab.put('?q={"idusuario": ' + req.params.id + '}&' + apiKey, JSON.parse(cambio), function(err, resM, body) {
    res.send(body)
  })
})



app.post('/v4/usuarios/login', function(req, res) {
  var nombre = req.headers.usuario
  var password= req.headers.password

    console.log(nombre)
    console.log(password)
    var usr;
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
    clienteMlab.get('?q={"idusuario":"' + nombre + '","password":"'+password+'"}&' + apiKey,
    function(err, resM, body) {
      if(body!=''){
        console.log(body[0].idusuario)


        if(body[0].idusuario!=''){
          res.send(body[0].idusuario )

        }else {
          res.send("usuario  no valido" )
        }
      }



      else {
        res.send("usuario "+nombre+" no valido" )
      }


    })
  /*var usr= usuariosJSON.filter(function(usuariosJSON){
  return (usuariosJSON.idusuario == nombre && usuariosJSON.password  == password)

});*/
  /*console.log(usr)
  if (usr != ''){
    //sconsole.log(usr[0])
    usr[0].conectado=true
    console.log(usr[0])
      res.send(usr[0])

  }else {
    res.send("usuario "+nombre+ " no encontrado")
  }*/

})



app.listen(3000)
console.log("Escuchando en el puerto 3000")
