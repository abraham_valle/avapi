var mocha = require ('mocha')
var chai = require ('chai')
var chaiHttp = require ('chai-http')

var should = chai.should()
var server = require ('../server.js')

chai.use(chaiHttp)//configuraciòn de Chai con el modulo https
describe('test de API usuarios',()=>{
  it('raiz de la API contesta', (done)=>{
    chai.request('http://localhost:3000')
      .get('/v3')
      .end((err, res)=>{
        //console.log(res)
        res.should.have.status(200)
        res.body.should.be.a('array')
        done()
      })
  })
  it('raiz de la API devuelve dos colecciones', (done)=>{
    chai.request('http://localhost:3000')
      .get('/v3')
      .end((err, res)=>{
        //console.log(res.body)
        res.should.have.status(200)
        res.body.should.be.a('array')
        res.body.length.should.be.eql(2)
        done()
      })
  })
  it('raiz de la API devuelve los objetos correctos', (done)=>{
    chai.request('http://localhost:3000')
      .get('/v3')
      .end((err, res)=>{
        //console.log(res.body)
        res.should.have.status(200)
        res.body.should.be.a('array')
        res.body.length.should.be.eql(2)
        for (var i = 0; i < res.body.length; i++) {
          res.body[i].should.have.property('recurso')
          res.body[i].should.have.property('url')
        }
        done()
      })
  })
  //prueba de que el api de movimientos tien eun arreglo
  it('Test de API movimientos', (done)=>{
    chai.request('http://localhost:3000')
      .get('/v3/movimientos')
      .end((err, res)=>{
        //console.log(res.body)
        res.should.have.status(200)
        res.body.should.be.a('array')

        done()
      })
  })
  //prueba de que devuelve un id
  it('Test de API movimientos', (done)=>{
    chai.request('http://localhost:3000')
      .get('/v3/movimientos/BH13 VJUC WMHT PFL4 12MV QN')
      .end((err, res)=>{
        console.log(res.body)
        res.should.have.status(200)
        res.body.should.be.a('array')
        res.body[0].idcuenta.should.equal('BH13 VJUC WMHT PFL4 12MV QN')

        done()
      })
  })
})
